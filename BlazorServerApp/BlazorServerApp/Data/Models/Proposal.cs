﻿namespace BlazorServerApp.Data.Models
{
    public enum ProposalStatus : byte
    {
        Created = 0,
        Approved = 1,
        Deleted = 2,
    }

    public class Proposal
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string FullNumber => String.Format("{0:d2}/{1:d4}", CreatedTime.Year % 100, Number);
        public string Author { get; set; }
        public string Department { get; set; }
        public DateTime CreatedTime { get; set; }
        public string TextStatus => new string[] { "Создана", "Утверждена", "Удалена" }[(int)Status];
        public ProposalStatus Status { get; set; }
    }
}
