﻿using System.ComponentModel.DataAnnotations;

namespace BlazorServerApp.Data.Models
{
    public enum MaterialStatus : byte
    {
        Created = 0,
        Deleted = 1,
    }

    public class ProposalMaterial
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        [StringLength(10, MinimumLength = 1)]
        public string Code { get; set; }

        [MinLength(1)]
        public int Count { get; set; }
        public string Comment { get; set; }
        public int ProposalId{ get; set; }
        public Proposal Proposal { get; set; }
        public MaterialStatus Status { get; set; }
        public string TextStatus => Status.ToString();
    }
}
