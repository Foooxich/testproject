﻿using BlazorServerApp.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;

namespace BlazorServerApp.Data
{
    public class PurchaseContext : DbContext
    {
        public DbSet<Proposal> Proposals { get; set; }
        public DbSet<ProposalMaterial> ProposalMaterials { get; set; }

        public PurchaseContext(DbContextOptions<PurchaseContext> options) 
            : base(options)
        { 
            
        }

    }
}
