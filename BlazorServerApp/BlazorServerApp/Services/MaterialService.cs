﻿using Azure.Core;
using BlazorServerApp.Data;
using BlazorServerApp.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace BlazorServerApp.Services
{
    public interface IMaterialInterface
    {
        public void Insert(ProposalMaterial material);
        public List<ProposalMaterial> GetMaterialsFor(int proposalId);
        public void Edit(ProposalMaterial material);
        public void Delete(ProposalMaterial material);
    }

    public class MaterialService : IMaterialInterface
    {
        private IDbContextFactory<PurchaseContext> dbContextFactory;

        public MaterialService(IDbContextFactory<PurchaseContext> dbContext)
        {
            dbContextFactory = dbContext;
        }

        public void Insert(ProposalMaterial material)
        {
            using var context = dbContextFactory.CreateDbContext();

            context.ProposalMaterials.Add(material);
            context.SaveChanges();
        }

        public void Edit(ProposalMaterial material)
        {
            using var context = dbContextFactory.CreateDbContext();

            context.Update(material);
            context.SaveChanges();
        }

        public void Delete(ProposalMaterial material)
        {
            using var context = dbContextFactory.CreateDbContext();

            material.Status = MaterialStatus.Deleted;

            context.Update(material);
            context.SaveChanges();
        }

        public List<ProposalMaterial> GetMaterialsFor(int proposalId)
        {
            using var context = dbContextFactory.CreateDbContext();

            return context.ProposalMaterials.Where((m) => m.ProposalId == proposalId && m.Status != MaterialStatus.Deleted).ToList();
        }
    }
}
