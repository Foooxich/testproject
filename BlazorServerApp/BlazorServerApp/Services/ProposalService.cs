﻿using BlazorServerApp.Data;
using BlazorServerApp.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace BlazorServerApp.Services
{
    public interface IProposalInterface
    {
        public List<Proposal> GetCreatedAndApprovedProposals();
        public Proposal GetProposalById(int Id);
        public void Insert(Proposal proposal);
        public void Edit(Proposal proposal);
        public void Approve(Proposal proposal);
        public void Delete(Proposal proposal);
    }

    public class ProposalService : IProposalInterface
    {
        private IDbContextFactory<PurchaseContext> dbContextFactory;

        public ProposalService(IDbContextFactory<PurchaseContext> dbContext)
        {
            dbContextFactory = dbContext;
        }

        public List<Proposal> GetCreatedAndApprovedProposals()
        {
            using var context = dbContextFactory.CreateDbContext();

            return context.Proposals.Where((p) => p.Status != ProposalStatus.Deleted).ToList();
        }

        public Proposal GetProposalById(int Id)
        {
            using var context = dbContextFactory.CreateDbContext();

            return context.Proposals.SingleOrDefault((p) => p.Id == Id  && p.Status != ProposalStatus.Deleted);
        }

        public void Insert(Proposal proposal)
        {
            using var context = dbContextFactory.CreateDbContext();

            proposal.Number = GetNumberForNewProposal();
            proposal.Status = ProposalStatus.Created;
            proposal.CreatedTime = DateTime.Now;

            context.Proposals.Add(proposal);
            context.SaveChanges();
        }

        private int GetNumberForNewProposal()
        {
            using var context = dbContextFactory.CreateDbContext();
            var proposals = 
                context.Proposals.Where(p => p.Status != ProposalStatus.Deleted)
                .Where(p => p.CreatedTime.Year == DateTime.Now.Year)
                .OrderBy(p => p.Number).ToArray();

            if (proposals.Length == 0) return 1;

            for (int i = 0; i < proposals.Length; i++)
            {
                int correctNumber = i + 1;

                if (proposals[i].Number != correctNumber)
                {
                    return correctNumber;
                }
            }

            return proposals.Last().Number + 1;
        }

        public void Edit(Proposal proposal)
        {
            using var context = dbContextFactory.CreateDbContext();

            context.Update(proposal);
            context.SaveChanges();
        }

        public void Approve(Proposal proposal)
        {
            ChangeStatus(proposal, ProposalStatus.Approved);
        }

        public void Delete(Proposal proposal)
        {
            ChangeStatus(proposal, ProposalStatus.Deleted);
        }

        private void ChangeStatus(Proposal proposal, ProposalStatus newStatus)
        {
            using var context = dbContextFactory.CreateDbContext();

            proposal.Status = newStatus;

            context.Update(proposal);
            context.SaveChanges();
        }
    }
}
